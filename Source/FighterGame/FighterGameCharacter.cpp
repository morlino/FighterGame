// Copyright Epic Games, Inc. All Rights Reserved.

#include "FighterGameCharacter.h"
#include "FighterGameGameMode.h"
#include "HitboxActor.h"
#include "BaseGameInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AFighterGameCharacter::AFighterGameCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 3.f;
	GetCharacterMovement()->AirControl = 1.0f;
	GetCharacterMovement()->JumpZVelocity = 1100.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;
	GetCharacterMovement()->bOrientRotationToMovement = false;

	otherPlayer = nullptr;
	hurtbox = nullptr;

	// Defaul Enums
	characterState = ECharacterState::VE_Default;

	// Vectors
	transform = FTransform();
	scale = FVector(0.0f, 0.0f, 0.0f);
	stunTimerHandler = FTimerHandle();

	// Character's info
	playerHealth = 1.00f;
	superMeterAmount = 0.0f;
	stunTime = 0.0f;
	maxDistanceApart = 600.0f;
	defaultGravityScale = GetCharacterMovement()->GravityScale;
	gravityScaleModifier = 0.7f;

	// Gmae Logic
	roundsWon = 0;

	// Default moves
	wasLeftPunchUsed = false;
	wasRightPunchUsed = false;
	wasLeftKickUsed = false;
	wasRightKickUsed = false;
	// Super
	wasSuperUsed = false;
	wasThrowUsed = false;
	// EX Moves
	wasLightExAttackUsed = false;
	wasMediumExAttackUsed = false;
	wasHeavyExAttackUsed = false;

	// Character state properties
	isFlipped = false;
	isCrouching = false;
	isPlayerOne = false;
	isReadyForEntrance = false;
	// Character action properties
	hasLandedHit = false;
	hasLandedThrow = false;
	wasThrown = false;
	hasReleasedAxisInput = true;
	hasLostRound = false;
	hasWonMatch = false;
	// Character abiltity properties
	canAttack = false;
	canFlip = true;
	canMove = false;
	canUseExAttack = true;


	// Create and assign number of character's commands
	characterCommands.SetNum(5);

	// Command #1 assignments.
	characterCommands[0].name = "Command #1";
	characterCommands[0].inputs.Add("S");
	characterCommands[0].inputs.Add("D");
	characterCommands[0].inputs.Add("U");
	characterCommands[0].hasUsedCommand = false;

	// Command #2 assignments.
	characterCommands[1].name = "Command #2";
	characterCommands[1].inputs.Add("S");
	characterCommands[1].inputs.Add("D");
	characterCommands[1].inputs.Add("K");
	characterCommands[1].hasUsedCommand = false;

	// Command #3 assignments.
	characterCommands[2].name = "Fireball";
	characterCommands[2].inputs.Add("S");
	characterCommands[2].inputs.Add("D");
	characterCommands[2].inputs.Add("I");
	characterCommands[2].hasUsedCommand = false;

	// Command #4 assignments.
	characterCommands[3].name = "Super";
	characterCommands[3].inputs.Add("S");
	characterCommands[3].inputs.Add("D");
	characterCommands[3].inputs.Add("A");
	characterCommands[3].hasUsedCommand = false;

	// Command #5 assignments.
	characterCommands[4].name = "Hurricane Kick";
	characterCommands[4].inputs.Add("S");
	characterCommands[4].inputs.Add("S");
	characterCommands[4].inputs.Add("J");
	characterCommands[4].hasUsedCommand = false;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFighterGameCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	if (auto gameMode = Cast<AFighterGameGameMode>(GetWorld()->GetAuthGameMode()))
	{
		if (gameMode->player1 == this)
		{
			PlayerInputComponent->BindAxis("MoveRightP1", this, &AFighterGameCharacter::MoveRight);
			isPlayerOne = true;
		}
		else
		{
			PlayerInputComponent->BindAxis("MoveRightP2", this, &AFighterGameCharacter::MoveRight);
			isPlayerOne = false;
		}

		// set up gameplay key bindings
		PlayerInputComponent->BindAction("JumpP1", IE_Pressed, this, &AFighterGameCharacter::Jump);
		PlayerInputComponent->BindAction("JumpP1", IE_Released, this, &AFighterGameCharacter::StopJumping);
		PlayerInputComponent->BindAction("CrouchP1", IE_Pressed, this, &AFighterGameCharacter::StartCrouching);
		PlayerInputComponent->BindAction("CrouchP1", IE_Released, this, &AFighterGameCharacter::StopCrouching);
		PlayerInputComponent->BindAxis("MoveRightController", this, &AFighterGameCharacter::MoveRightController);

		PlayerInputComponent->BindAction("Attack1P1", IE_Pressed, this, &AFighterGameCharacter::StartAttack1);
		// PlayerInputComponent->BindAction("Attack1P1", IE_Released, this, &AFighterGameCharacter::StartAttack1);
		PlayerInputComponent->BindAction("Attack2P1", IE_Pressed, this, &AFighterGameCharacter::StartAttack2);
		// PlayerInputComponent->BindAction("Attack2P1", IE_Released, this, &AFighterGameCharacter::StartAttack2);
		PlayerInputComponent->BindAction("Attack3P1", IE_Pressed, this, &AFighterGameCharacter::StartAttack3);
		// PlayerInputComponent->BindAction("Attack3P1", IE_Released, this, &AFighterGameCharacter::StartAttack3);
		PlayerInputComponent->BindAction("Attack4P1", IE_Pressed, this, &AFighterGameCharacter::StartAttack4);
		// PlayerInputComponent->BindAction("Attack4P1", IE_Released, this, &AFighterGameCharacter::StartAttack4);

		PlayerInputComponent->BindAction("ExceptionalAttackP1", IE_Pressed, this, &AFighterGameCharacter::StartExceptionalAttack);
		PlayerInputComponent->BindAction("ThrowP1", IE_Pressed, this, &AFighterGameCharacter::StartThrow);
	}
}

void AFighterGameCharacter::Jump()
{
	if (canMove &&
		characterState != ECharacterState::VE_Stunned &&
		characterState != ECharacterState::VE_Launched &&
		characterState != ECharacterState::VE_KnockedDown &&
		characterState != ECharacterState::VE_Recovery)
	{
		if (characterState == ECharacterState::VE_MovingLeft)
		{
			AddInputIconToScreen(9);
		}
		else if (characterState == ECharacterState::VE_MovingRight)
		{
			AddInputIconToScreen(10);
		}
		else
		{
			AddInputIconToScreen(0);
		}
		ACharacter::Jump();
		characterState = ECharacterState::VE_Jumping;
	}
	else if (characterState == ECharacterState::VE_KnockedDown)
	{
		characterState = ECharacterState::VE_Recovery;
	}
}

void AFighterGameCharacter::StopJumping()
{
	ACharacter::StopJumping();
}

void AFighterGameCharacter::Landed(const FHitResult& Hit)
{
	if (characterState == ECharacterState::VE_Jumping)
	{
		if (!Cast<AHitboxActor>(Hit.Actor.Get()))
		{
			GetCharacterMovement()->GravityScale = defaultGravityScale;
			gravityScaleModifier = BASE_GRAVITY_MODIFIER;
			characterState = ECharacterState::VE_Default;
		}
		
	}
	else if (characterState == ECharacterState::VE_Launched)
	{
		if (!Cast<AHitboxActor>(Hit.Actor.Get()))
		{
			GetCharacterMovement()->GravityScale = defaultGravityScale;
			gravityScaleModifier = BASE_GRAVITY_MODIFIER;
			characterState = ECharacterState::VE_KnockedDown;
		}
	}
}

void AFighterGameCharacter::StartCrouching()
{
	if (canMove &&
		characterState != ECharacterState::VE_Stunned &&
		characterState != ECharacterState::VE_Jumping &&
		characterState != ECharacterState::VE_Launched &&
		characterState != ECharacterState::VE_Recovery)
	{
		Crouch();
		isCrouching = true;
		AddInputIconToScreen(2);
	}
}

void AFighterGameCharacter::StopCrouching()
{
	if (characterState != ECharacterState::VE_Stunned &&
		characterState != ECharacterState::VE_Jumping &&
		characterState != ECharacterState::VE_Launched &&
		characterState != ECharacterState::VE_Recovery)
	{
		UnCrouch();
		isCrouching = false;
	}
}

void AFighterGameCharacter::MoveRight(float Value)
{
	if (auto baseGameInstance = Cast<UBaseGameInstance>(GetGameInstance()))
	{
		if (baseGameInstance->isDeviceForMultiplePlayers)
		{
			if (canMove &&
				!isCrouching &&
				characterState != ECharacterState::VE_Blocking &&
				characterState != ECharacterState::VE_Stunned &&
				characterState != ECharacterState::VE_KnockedDown &&
				characterState != ECharacterState::VE_Recovery)
			{
				if (characterState != ECharacterState::VE_Jumping && characterState != ECharacterState::VE_Launched)
				{
					if (Value > 0.01f)
					{
						characterState = ECharacterState::VE_MovingRight;
						AddInputIconToScreen(1, hasReleasedAxisInput);
						hasReleasedAxisInput = false;
					}
					else if (Value < -0.01f)
					{
						characterState = ECharacterState::VE_MovingLeft;
						AddInputIconToScreen(3, hasReleasedAxisInput);
						hasReleasedAxisInput = false;
					}
					else
					{
						characterState = ECharacterState::VE_Default;
						hasReleasedAxisInput = true;
					}
				}

				float currentDistanceApart = abs(otherPlayer->GetActorLocation().Y - GetActorLocation().Y);

				if (currentDistanceApart >= maxDistanceApart)
				{
					if (currentDistanceApart + Value > currentDistanceApart && !isFlipped || currentDistanceApart - Value > currentDistanceApart && isFlipped)
					{
						// add movement in that direction
						AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
					}
				}
				else
				{
					// add movement in that direction
					AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
				}
			}
			// Crouching + Directional input
			else if (canMove && isCrouching)
			{
				if (Value > 0.01f)
				{
					AddInputIconToScreen(12, hasReleasedAxisInput);
					hasReleasedAxisInput = false;
				}
				else if (Value < -0.01f)
				{
					AddInputIconToScreen(11, hasReleasedAxisInput);
					hasReleasedAxisInput = false;
				}
				else
				{
					hasReleasedAxisInput = true;
				}
			}
		}
	}

}

void AFighterGameCharacter::MoveRightController(float Value)
{
	if (auto baseGameInstance = Cast<UBaseGameInstance>(GetGameInstance()))
	{
		// If it's NOT a device for multiple players
		if (!baseGameInstance->isDeviceForMultiplePlayers)
		{
			if (canMove &&
				!isCrouching &&
				characterState != ECharacterState::VE_Blocking &&
				characterState != ECharacterState::VE_Stunned &&
				characterState != ECharacterState::VE_KnockedDown &&
				characterState != ECharacterState::VE_Recovery)
			{
				if (characterState != ECharacterState::VE_Jumping && characterState != ECharacterState::VE_Launched)
				{
					if (Value > 0.20f)
					{
						characterState = ECharacterState::VE_MovingRight;
						AddInputIconToScreen(1, hasReleasedAxisInput);
						hasReleasedAxisInput = false;
					}
					else if (Value < -0.20f)
					{
						characterState = ECharacterState::VE_MovingLeft;
						AddInputIconToScreen(3, hasReleasedAxisInput);
						hasReleasedAxisInput = false;
					}
					else
					{
						characterState = ECharacterState::VE_Default;
						hasReleasedAxisInput = true;
					}
				}

				float currentDistanceApart = abs(otherPlayer->GetActorLocation().Y - GetActorLocation().Y);

				if (currentDistanceApart >= maxDistanceApart)
				{
					if (currentDistanceApart + Value > currentDistanceApart && !isFlipped || currentDistanceApart - Value > currentDistanceApart && isFlipped)
					{
						// add movement in that direction
						AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
					}
				}
				else
				{
					// add movement in that direction
					AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
				}
			}
			// Crouching + Directional input
			else if (canMove && isCrouching)
			{
				if (Value > 0.20f)
				{
					AddInputIconToScreen(12, hasReleasedAxisInput);
					hasReleasedAxisInput = false;
				}
				else if (Value < -0.20f)
				{
					AddInputIconToScreen(11, hasReleasedAxisInput);
					hasReleasedAxisInput = false;
				}
				else
				{
					hasReleasedAxisInput = true;
				}
			}
		}
	}
}

void AFighterGameCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// jump on any touch
	Jump();
}

void AFighterGameCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	StopJumping();
}

void AFighterGameCharacter::StartAttack1()
{
	if (canAttack &&
		characterState != ECharacterState::VE_Stunned &&
		characterState != ECharacterState::VE_Blocking)
	{
		if (isFlipped)
		{
			UE_LOG(LogTemp, Warning, TEXT("Is flipped."));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Is not flipped."));
		}
		//UE_LOG(LogTemp, Warning, TEXT("We are using our first sttack."));
		wasLeftPunchUsed = true;
		AddInputIconToScreen(4);
	}
}

void AFighterGameCharacter::StartAttack2()
{
	if (canAttack &&
		characterState != ECharacterState::VE_Stunned &&
		characterState != ECharacterState::VE_Blocking)
	{
		//UE_LOG(LogTemp, Warning, TEXT("We are using our second sttack."));
		wasRightPunchUsed = true;
		AddInputIconToScreen(5);
	}
}

void AFighterGameCharacter::StartAttack3()
{
	if (canAttack &&
		characterState != ECharacterState::VE_Stunned &&
		characterState != ECharacterState::VE_Blocking)
	{
		//UE_LOG(LogTemp, Warning, TEXT("We are using our third sttack."));
		wasLeftKickUsed = true;
		AddInputIconToScreen(6);
	}
}

void AFighterGameCharacter::StartAttack4()
{
	if (canAttack &&
		characterState != ECharacterState::VE_Stunned &&
		characterState != ECharacterState::VE_Blocking)
	{
		//UE_LOG(LogTemp, Warning, TEXT("We are using our fourth sttack."));
		wasRightKickUsed = true;
		AddInputIconToScreen(7);
		//if (superMeterAmount >= 1.0f)
		//{
		//	wasSuperUsed = true;
		//}
	}
}

void AFighterGameCharacter::StartThrow()
{
	if (canAttack && characterState != ECharacterState::VE_Stunned)
	{
		//if (wasLeftPunchUsed || wasRightPunchUsed)
		//{
		//	UE_LOG(LogTemp, Warning, TEXT("The character uses throw."));
		//	wasThrowUsed = true;
		//}
		UE_LOG(LogTemp, Warning, TEXT("The character uses throw."));
		wasThrowUsed = true;
	}
}

void AFighterGameCharacter::StartExceptionalAttack()
{
	////AddInputIconToScreen(8);
	//UE_LOG(LogTemp, Warning, TEXT("StartExceptionalAttack."));
	//if (wasLeftPunchUsed)
	//{
	//	AddInputIconToScreen(13);
	//	if (superMeterAmount >= 0.20f && canUseExAttack)
	//	{
	//		wasLightExAttackUsed = true;
	//		superMeterAmount -= 0.20f;
	//	}
	//}
	//else if (wasRightPunchUsed)
	//{
	//	AddInputIconToScreen(14);
	//	if (superMeterAmount >= 0.35f && canUseExAttack)
	//	{
	//		wasMediumExAttackUsed = true;
	//		superMeterAmount -= 0.35f;
	//	}	
	//}
	//else if (wasLeftKickUsed)
	//{
	//	AddInputIconToScreen(15);
	//	if (superMeterAmount >= 0.50f && canUseExAttack)
	//	{
	//		wasHeavyExAttackUsed = true;
	//		superMeterAmount -= 0.50f;
	//	}
	//}

	//if (superMeterAmount < 0.00f)
	//{
	//	superMeterAmount = 0.00f;
	//}
}

void AFighterGameCharacter::P2KeyboardAttack1()
{
	StartAttack1();
}

void AFighterGameCharacter::P2KeyboardAttack2()
{
	StartAttack2();
}

void AFighterGameCharacter::P2KeyboardAttack3()
{
	StartAttack3();
}

void AFighterGameCharacter::P2KeyboardAttack4()
{
	StartAttack4();
}

void AFighterGameCharacter::P2KeyboardExceptionalAttack()
{
	UE_LOG(LogTemp, Warning, TEXT("The player pressed Ex input."));
	StartExceptionalAttack();
}

void AFighterGameCharacter::P2KeyboardThrow()
{
	UE_LOG(LogTemp, Warning, TEXT("The player pressed Ex input."));
	StartThrow();
}

void AFighterGameCharacter::P2KeyboardJump()
{
	Jump();
}

void AFighterGameCharacter::P2KeyboardStopJumping()
{
	StopJumping();
}

void AFighterGameCharacter::P2KeyboardMoveRight(float _value)
{
	MoveRight(_value);
}

void AFighterGameCharacter::CollidedWIthProximityHitbox()
{
	if ((characterState == ECharacterState::VE_MovingLeft && !isFlipped) || (characterState == ECharacterState::VE_MovingRight && isFlipped))
	{
		UE_LOG(LogTemp, Warning, TEXT("The character is auto-blocking."));
		characterState = ECharacterState::VE_Blocking;
	}
}

void AFighterGameCharacter::TakeDamage(float _damageAmount, float _hitstunTime, float _blockstunTime, float _pushbackAmount, float _launchAmount)
{
	// Hit landed
	if(characterState != ECharacterState::VE_Blocking)
	{
		//UE_LOG(LogTemp, Warning, TEXT("We are taking damage for %f points."), _damageAmount);
		playerHealth -= _damageAmount;
		// Gein super meter
		superMeterAmount += _damageAmount * 0.85f;

		PlayDamageSoundEffect();

		stunTime = _hitstunTime;

		if (stunTime > 0.0f)
		{
			characterState = ECharacterState::VE_Stunned;
			BeginStun();
		}
		else
		{
			characterState = ECharacterState::VE_Default;
		}

		if (otherPlayer)
		{
			otherPlayer->hasLandedHit = true;
			otherPlayer->PerformePushback(_pushbackAmount, 0.0f, false);

			if (!otherPlayer->wasLightExAttackUsed && !otherPlayer->wasMediumExAttackUsed && !otherPlayer->wasHeavyExAttackUsed)
			{
				otherPlayer->superMeterAmount += _damageAmount * 0.30f;
			}
		}

		PerformePushback(_pushbackAmount, _launchAmount, false);
	}
	// Hit blocked
	else
	{
		float reducedDamage = _damageAmount * 0.5f;
		UE_LOG(LogTemp, Warning, TEXT("We are taking reduced damage for %f points."), reducedDamage);
		playerHealth -= reducedDamage;

		stunTime = _blockstunTime;

		if (stunTime > 0.0f)
		{
			BeginStun();
		}
		else
		{
			characterState = ECharacterState::VE_Default;
		}
		if (otherPlayer)
		{
			otherPlayer->hasLandedHit = false;
			otherPlayer->PerformePushback(_pushbackAmount, 0.0f, false);
		}

		PerformePushback(_pushbackAmount, 0.0f, true);

	}

	if (playerHealth <= 0.00f)
	{
		playerHealth = 0.00f;
		otherPlayer->WinRound();
	}
	else if (playerHealth > 0.0f && playerHealth < 0.5f)
	{
		//ChangeToDamageMaterials();
	}
}

void AFighterGameCharacter::PerformePushback(float _pushbackAmount, float _launchAmount, bool _hasBlocked)
{
	if (_hasBlocked)
	{
		if (isFlipped)
		{
			LaunchCharacter(FVector(0.0f, _pushbackAmount * 2.0f, 0.0f), false, false);
		}
		else
		{
			LaunchCharacter(FVector(0.0f, -_pushbackAmount * 2.0f, 0.0f), false, false);
		}
	}
	else
	{
		if (_launchAmount > 0.0f)
		{
			GetCharacterMovement()->GravityScale = defaultGravityScale * gravityScaleModifier;
			gravityScaleModifier += 1.0f;
			characterState = ECharacterState::VE_Launched;
		}

		if (isFlipped)
		{
			LaunchCharacter(FVector(0.0f, _pushbackAmount, _launchAmount), false, false);
		}
		else
		{
			LaunchCharacter(FVector(0.0f, -_pushbackAmount, _launchAmount), false, false);
		}
	}
}

void AFighterGameCharacter::BeginStun()
{
	UE_LOG(LogTemp, Warning, TEXT("Stun Began."));
	canMove = false;
	GetWorld()->GetTimerManager().SetTimer(stunTimerHandler, this, &AFighterGameCharacter::EndStun, stunTime, false);
}

void AFighterGameCharacter::EndStun()
{
	UE_LOG(LogTemp, Warning, TEXT("Stun Ended."));
	if(characterState != ECharacterState::VE_Launched)
	{
		characterState = ECharacterState::VE_Default;
	}

	canMove = true;
}

void AFighterGameCharacter::AddInputToInputBuffer(FInputInfo _inputInfo)
{

	inputBuffer.Add(_inputInfo);
	CheckInputBufferForCommand();
}

void AFighterGameCharacter::CheckInputBufferForCommand()
{
	// Counts how many inputs in input buffer match the commands.
	int correctSequenceCounter = 0;

	// Loop through all moves in the command list
	for (auto currentCommand : characterCommands)
	{
		// Check if there's more inputs in buffer than inputs in the commands.
		for (int commandInput = 0; commandInput < currentCommand.inputs.Num(); ++commandInput)
		{
			// For loop input buffer
			for (int input = 0; input < inputBuffer.Num(); ++input)
			{
				// Size Check to avoid crash (don't go over an array length).
				if (input + correctSequenceCounter < inputBuffer.Num())
				{
					// Compare input from input buffer to inputs from Commands to check if it's valid.
					if (inputBuffer[input + correctSequenceCounter].inputName.Compare(currentCommand.inputs[commandInput]) == 0)
					{
						// If input is valid, increase correct sequence counter.
						//UE_LOG(LogTemp, Warning, TEXT("The player  added another input to command sequence: %s."), *currentCommand.name);
						++correctSequenceCounter;

						if (correctSequenceCounter == currentCommand.inputs.Num())
						{
							StartCommand(currentCommand.name);
						}

						break;
					}
					else
					{
						// Reset counter if a wrong input was pressed
						//UE_LOG(LogTemp, Warning, TEXT("The player broke the command sequence."));
						correctSequenceCounter = 0;
					}
				}
				else
				{
					// Reset counter if not enough inputs
					//UE_LOG(LogTemp, Warning, TEXT("The player hasn't finished the command sequence yet."));
					correctSequenceCounter = 0;
				}
			}
		}
	}
}

void AFighterGameCharacter::StartCommand(FString _commandName)
{
	UE_LOG(LogTemp, Warning, TEXT("Correct input."));
	for (int currentCommand = 0; currentCommand < characterCommands.Num(); ++currentCommand)
	{
		if (_commandName.Compare(characterCommands[currentCommand].name) == 0)
		{
			if (canAttack)
			{
				UE_LOG(LogTemp, Warning, TEXT("The character is using the command %s."), *_commandName);
				characterCommands[currentCommand].hasUsedCommand = true;
			}
		}
	}
}

void AFighterGameCharacter::SetHealth(float _health)
{
	playerHealth = _health;
}

void AFighterGameCharacter::SetSuperMeter(float _superMeter)
{
	superMeterAmount = _superMeter;
}

void AFighterGameCharacter::WinRound()
{
	if (!otherPlayer->hasLostRound)
	{
		otherPlayer->hasLostRound = true;
		++roundsWon;

		NotifyRoundEnd();
		UpdateHUDRoundIcons();
	}
}

void AFighterGameCharacter::WinMatch()
{
	canMove = false;
	canAttack = false;
	hasWonMatch = true;
}

void AFighterGameCharacter::Draw()
{
	hasLostRound = true;
	otherPlayer->hasLostRound = true;
	++roundsWon;
	++(otherPlayer->roundsWon);

	NotifyRoundEnd();
	UpdateHUDRoundIcons();
}

void AFighterGameCharacter::CheatTakeDamage(float _damageAmount)
{
	TakeDamage(_damageAmount, 0.0f, 0.0f, 0.0f, 0.0f);
}

void AFighterGameCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// If not is the air
	if (characterState != ECharacterState::VE_Jumping && canFlip)
	{
		// If enemy is valid
		if (otherPlayer)
		{
			//if (otherPlayer->GetActorLocation().Y <= GetActorLocation().Y)
			if (GetActorLocation().Y <= otherPlayer->GetActorLocation().Y)
			{
				// Flip mash if facing left
				if (isFlipped)
				{
					GetCapsuleComponent()->GetChildrenComponents(true, capsuleChildren);

					for (auto child : capsuleChildren)
					{
						if (child->GetName().Contains("CharacterMesh"))
						{
							characterMesh = child;
							break;
						}
					}

					if (characterMesh)
					{
						transform = characterMesh->GetRelativeTransform();
						scale = transform.GetScale3D();
						scale.Y = 1.0f;
						transform.SetScale3D(scale);

						characterMesh->SetRelativeTransform(transform);
					}
					isFlipped = false;
				}
			}
			// If P1 is on the left side
			else
			{
				// Flip mash if facing right
				if (!isFlipped)
				{
					GetCapsuleComponent()->GetChildrenComponents(true, capsuleChildren);

					for (auto child : capsuleChildren)
					{
						if (child->GetName().Contains("CharacterMesh"))
						{
							characterMesh = child;
							break;
						}
					}

					if (characterMesh)
					{
						transform = characterMesh->GetRelativeTransform();
						scale = transform.GetScale3D();
						scale.Y = -1.0f;
						transform.SetScale3D(scale);

						characterMesh->SetRelativeTransform(transform);
					}
					isFlipped = true;
				}
			}
		}
	}
}