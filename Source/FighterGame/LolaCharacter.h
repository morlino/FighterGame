// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FighterGameCharacter.h"
#include "LolaCharacter.generated.h"

/**
 * 
 */
UCLASS()
class FIGHTERGAME_API ALolaCharacter : public AFighterGameCharacter
{
	GENERATED_BODY()
public:
	ALolaCharacter();
};
