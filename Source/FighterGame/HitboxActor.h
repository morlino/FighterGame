// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HitboxActor.generated.h"

UENUM(BlueprintType)
enum class EHitboxEnum : uint8
{
	HB_PROXIMITY	UMETA(DisplayName = "Proximity"),
	HB_STRIKE		UMETA(DisplayName = "Strike"),
	HB_THROW		UMETA(DisplayName = "Throw"),
	HB_HURTBOX		UMETA(DisplayName = "Hurtbox")
};

UCLASS()
class FIGHTERGAME_API AHitboxActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHitboxActor();

	// Notify the hitboxActorBP class instance that the hitbox is ready to be drawn.
	UFUNCTION(BlueprintImplementableEvent)
	void TriggerVisualizeHitbox();

	// Draw the hitbox to the screen to visualize it (for developers only).
	UFUNCTION(BlueprintImplementableEvent)
	void VisualizeHitbox();

	// The hitbox enum instance.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
	EHitboxEnum hitboxType;

	// The location to spawn the hitbox.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
	FVector hitboxLocation;

	// The damage this hitbox will do.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
	float hitboxDamage;

	// The amount of time hitstun will last.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
	float hitstunTime;

	// The amount of time hitstun will last.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
	float blockstunTime;

	// The amount of distance to the player back.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
	float pushbackDistance;

	// The maount of distance the player will be launched.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
	float launchDistance;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
