// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once
#include "CoreMinimal.h"
#include "BaseGameInstance.h"
#include "GameFramework/Character.h"
#include "FighterGameCharacter.generated.h"

#define BASE_GRAVITY_MODIFIER 0.7f

UENUM(BlueprintType)
enum class ECharacterState : uint8
{
	VE_Default		UMETA(DisplayName = "NOT_MOVING"),
	VE_MovingRight	UMETA(DisplayName = "MOVING_RIGHT"),
	VE_RunningRight	UMETA(DisplayName = "RUNNING_RIGHT"),
	VE_MovingLeft	UMETA(DisplayName = "MOVING_LEFT"),
	VE_RunningLeft	UMETA(DisplayName = "RUNNING_LEFT"),
	VE_Jumping		UMETA(DisplayName = "JUMPING"),
	VE_Stunned		UMETA(DisplayName = "STUNNED"),
	VE_Blocking		UMETA(DisplayName = "BLOCKING"),
	VE_Launched		UMETA(DisplayName = "LAUNCHED"),
	VE_KnockedDown	UMETA(DisplayName = "KNOCKED DOWN"),
	VE_Recovery		UMETA(DisplayName = "RECOVERY")
};

USTRUCT(BlueprintType)
struct FCommand
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
	FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
	TArray<FString> inputs;

	// Command Boolean.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Commands")
	bool hasUsedCommand;
};

USTRUCT(BlueprintType)
struct FInputInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
	FString inputName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
	float timeStamp;
};

UCLASS(config=Game)
class AFighterGameCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Side view camera */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class USpringArmComponent* CameraBoom;

	void StartAttack1();
	void StartAttack2();
	void StartAttack3();
	void StartAttack4();
	void StartThrow();
	void StartExceptionalAttack();

	// When is Keyboard-Only Mode, use these functions to performe actions with Player 2.
	UFUNCTION(BlueprintCallable)
	void P2KeyboardAttack1();

	UFUNCTION(BlueprintCallable)
	void P2KeyboardAttack2();

	UFUNCTION(BlueprintCallable)
	void P2KeyboardAttack3();

	UFUNCTION(BlueprintCallable)
	void P2KeyboardAttack4();

	UFUNCTION(BlueprintCallable)
	void P2KeyboardExceptionalAttack();

	UFUNCTION(BlueprintCallable)
	void P2KeyboardThrow();

	UFUNCTION(BlueprintCallable)
	void P2KeyboardJump();

	UFUNCTION(BlueprintCallable)
	void P2KeyboardStopJumping();

	UFUNCTION(BlueprintCallable)
	void P2KeyboardMoveRight(float _value);

protected:

	// Used to move character left and right (using button controls, such as a keyboard).
	void MoveRight(float Val);

	// Used to move character left and right (using joystick controls, such as analog sticks on controller).
	void MoveRightController(float _val);

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	// Called every frame.
	virtual void Tick(float DeltaTime)override;

	// Override the ACharacter and APawn functionality to have more control over the jumping the landing.
	virtual void Jump() override;
	virtual void StopJumping() override;
	virtual void Landed(const FHitResult& Hit) override;

	// Make the charcter begin crouching.
	UFUNCTION(BlueprintCallable)
	void StartCrouching();

	// Make the charcter stop crouching.
	UFUNCTION(BlueprintCallable)
	void StopCrouching();

	// Determine what the character should do when collieding with proximity hitbox.
	UFUNCTION(BlueprintCallable)
	void CollidedWIthProximityHitbox();

	// Damage the player.
	UFUNCTION(BlueprintCallable)
	void TakeDamage(float _damageAmount, float _hitstunTime, float _blockstunTime, float _pushbackAmount, float _launchAmount);

	// Determine how far the character should be pushed back.
	UFUNCTION(BlueprintCallable)
	void PerformePushback(float _pushbackAmount, float _launchAmount, bool _hasBlocked);

	// Display damage on character, when taking a hit.
	UFUNCTION(BlueprintImplementableEvent)
	void ChangeToDamageMaterials();

	// Enter the stun state.
	void BeginStun();

	// Exit the stun state.
	void EndStun();

	// Add inputs to the input buffer.
	UFUNCTION(BlueprintCallable)
	void AddInputToInputBuffer(FInputInfo _inputInfo);

	// Check if input buffer contains any seuquences from the character's command list.
	UFUNCTION(BlueprintCallable)
	void CheckInputBufferForCommand();

	// Make the character begin using a command state based off of the command's name.
	UFUNCTION(BlueprintCallable)
	void StartCommand(FString _commandName);

	// Character wins the round
	UFUNCTION(BlueprintCallable)
	void WinRound();

	// Character wins the match
	UFUNCTION(BlueprintCallable)
	void WinMatch();

	// Character wins the round
	UFUNCTION(BlueprintCallable)
	void Draw();

	UFUNCTION(BlueprintImplementableEvent)
	void AddInputIconToScreen(int _iconIndex, bool _shouldAddInput = true);

	// Plays the sound effect when character takes a hit.
	UFUNCTION(BlueprintImplementableEvent)
	void PlayDamageSoundEffect();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void NotifyRoundStart();

	UFUNCTION(BlueprintImplementableEvent)
	void NotifyRoundEnd();

	UFUNCTION(BlueprintImplementableEvent)
	void UpdateHUDRoundIcons();

	// The array of inputs the player has currently perforemed.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
	TArray<FInputInfo> inputBuffer;
	
	// Commands to be used when the serious of inputs has been pressed.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input")
	TArray<FCommand> characterCommands;

	// A pointer to the other player
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player References")
	AFighterGameCharacter* otherPlayer;

	// The hurtbox of a player
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
	AActor* hurtbox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	TArray<USceneComponent*> capsuleChildren;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	USceneComponent* characterMesh;

	// The current state of the character (moving, blocking, crouching, etc.)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Details")
	ECharacterState characterState;

	// The current class of the character (Mannequin, Lola, etc.)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Details")
	ECharacterClass characterClass;

	// The character's transform
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Model")
	FTransform transform;

	// The character's scale
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Model")
	FVector scale;

	// The timer handle for all stuns.
	FTimerHandle stunTimerHandler;

	// The amount of health the character currently has.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float playerHealth;

	// The maximum amount of distance that the players can be apart.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float maxDistanceApart;

	// The amount of time the character will be stunned.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float stunTime;

	// the scale value for gravity.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float defaultGravityScale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float gravityScaleModifier;

	// The amount of super meter the player has.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Super Meter")
	float superMeterAmount;

	// The amount of time before inputs are removed from the input buffer.
	float removeInputFromBufferTime;

	// The amount of rounds won by the character.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game Logic")
	int roundsWon;

	// Has the player used the light attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasLeftPunchUsed;

	// Has the player used the medium attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasRightPunchUsed;

	// Has the player used the heavy attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasLeftKickUsed;

	// Has the player used the heavy attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasRightKickUsed;

	// Has the player used the super attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasSuperUsed;

	// Has the player perform the throw move?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasThrowUsed;

	// Has the player used the light excpetional attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasLightExAttackUsed;

	// Has the player used the medium excpetional attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasMediumExAttackUsed;

	// Has the player used the heavy excpetional attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasHeavyExAttackUsed;

	// Can the player use excpetional attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool canUseExAttack;

	// Is the character's model flipped?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Model")
	bool isFlipped;

	// Is the character crouching?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool isCrouching;

	// Has the player landed a hit with their last attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool hasLandedHit;

	// Has the player landed a throw with their last attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool hasLandedThrow;

	// has the player been thrown?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool wasThrown;

	// Is the player currently able to perform an attack?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
	bool canAttack;

	// Is the character currently able to flip?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	bool canFlip;

	// Is the player currently able to move?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool canMove;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Controller")
	bool isDeviceForMultiplePlayers;

	// Has the user released a directional input?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input Stack")
	bool hasReleasedAxisInput;

	// Is the character controlled by Player 1?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input Stack")
	bool isPlayerOne;

	// Is the character ready to perform their entrance animation?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	bool isReadyForEntrance;

	// Is this character knocked down due to round-loss?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	bool hasLostRound;

	// Is this character won the match?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	bool hasWonMatch;

public:
	AFighterGameCharacter();

	// Set the player's health to a specifc value
	void SetHealth(float _health);

	// Set the player's super meter to a specifc value
	void SetSuperMeter(float _superMeter);

	// Cheat: Damage the player.
	void CheatTakeDamage(float _damageAmount);

	/** Returns SideViewCameraComponent subobject **/
	//FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	//FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
};
